import {
  JupyterFrontEnd,
  JupyterFrontEndPlugin
} from '@jupyterlab/application';

import { IThemeManager } from '@jupyterlab/apputils';

import { ISettingRegistry } from '@jupyterlab/settingregistry';

/**
 * Initialization data for the lnb_rw_theme extension.
 */
const plugin: JupyterFrontEndPlugin<void> = {
  id: 'lnb_rw_theme:plugin',
  autoStart: true,
  requires: [IThemeManager],
  optional: [ISettingRegistry],
  activate: (app: JupyterFrontEnd, themeManager: IThemeManager, settingRegistry: ISettingRegistry | null) => {
    const style = 'lnb_rw_theme/index.css';

    themeManager.register({
      name: 'lnb_rw_theme',
      isLight: true,
      load: () => themeManager.loadCSS(style),
      unload: () => Promise.resolve(undefined)
    });

    const id_ld: any = window.frameElement.id.split('ld_code_iframe_').pop();

    if (settingRegistry) {
      settingRegistry
        .load(plugin.id)
        .then(settings => {
          console.log('lnb_rw_theme: settings loaded [id_labdoc='+id_ld+'].', settings.composite);
        })
        .catch(reason => {
          console.error('lnb_rw_theme: failed to load settings [id_labdoc='+id_ld+'].', reason);
        });
    }

    console.log("lnb_rw_theme -> lnb: lnb_rw_theme is registered [id_labdoc="+id_ld+"]");

  }
};

export default plugin;
